# Information / Информация

Интеграция комментариев в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Comments`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Comments' );
```

## Syntax / Синтаксис

```html
{{#comments: [TYPE]|[ID]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
